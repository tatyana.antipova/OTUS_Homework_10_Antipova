using OTUS_Homework_10_Antipova_prototype_pattern.Models;
using OTUS_Homework_10_Antipova_prototype_pattern.Tests.Stubs;
using System.Text.Json;

namespace OTUS_Homework_10_Antipova_prototype_pattern.Tests;

[Trait("Category", "Unit")]
public class DiplomaTests
{
    [Fact]
    public void Clone_Serialize_ShouldBeEqual()
    {
        var testObject = DiplomaStub.Create();

        var testObjectCloned = testObject.Clone();
        var testObjectMyCloned = testObject.MyClone();

        string diplomaSerialized = JsonSerializer.Serialize(testObject);
        Assert.True(diplomaSerialized.Equals(JsonSerializer.Serialize(testObjectCloned)));
        Assert.True(diplomaSerialized.Equals(JsonSerializer.Serialize(testObjectMyCloned)));
    }

    [Fact]
    public void Clone_ObjectReferences_ShouldNotBeEqual()
    {
        var testObject = DiplomaStub.Create();

        var testObjectCloned = testObject.Clone();
        var testObjectMyCloned = testObject.MyClone();

        Assert.True(testObject != testObjectCloned);
        Assert.True(testObject != testObjectMyCloned);
    }

    [Fact]
    public void Clone_ObjectFields_BeforeChange_ShouldBeEqual()
    {
        var testObject = DiplomaStub.Create();

        var testObjectCloned = testObject.Clone();
        var testObjectMyCloned = testObject.MyClone();

        Assert.True(testObject.CourseName.Equals(((Diploma)testObjectCloned).CourseName));
        Assert.True(testObject.CourseName.Equals((testObjectMyCloned).CourseName));

        Assert.True(testObject.Degree.Equals(((Diploma)testObjectCloned).Degree));
        Assert.True(testObject.Degree.Equals((testObjectMyCloned).Degree));
    }

    [Fact]
    public void Clone_ObjectFields_AfterChange_ShouldNotBeEqual()
    {
        var testObject = DiplomaStub.Create();

        testObject.CourseName = "old string - 1";
        testObject.Degree = "old string - 2";

        var testObjectCloned = testObject.Clone();
        var testObjectMyCloned = testObject.MyClone();

        testObject.CourseName = "some new string - 1";
        testObject.Degree = "some new string - 2";

        Assert.False(testObject.CourseName.Equals(((Diploma)testObjectCloned).CourseName));
        Assert.False(testObject.CourseName.Equals((testObjectMyCloned).CourseName));

        Assert.False(testObject.Degree.Equals(((Diploma)testObjectCloned).Degree));
        Assert.False(testObject.Degree.Equals((testObjectMyCloned).Degree));
    }    
}