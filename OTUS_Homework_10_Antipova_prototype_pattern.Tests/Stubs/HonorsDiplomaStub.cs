﻿using OTUS_Homework_10_Antipova_prototype_pattern.Models;

namespace OTUS_Homework_10_Antipova_prototype_pattern.Tests.Stubs;

public static class HonorsDiplomaStub
{
    public static HonorsDiploma Create() =>
        new HonorsDiploma()
        {
            Number = "ОТЛ-52 552298",
            CourseName = "Обработка металлов давлением",
            StudentFullName = "Белякова Инга Олеговна",
            StartDate = new DateTime(2023, 06, 26),
            Degree = "Бакалавр",
            FinalExamScore = 100,
            OfficialSealAvailable = false
        };
}
