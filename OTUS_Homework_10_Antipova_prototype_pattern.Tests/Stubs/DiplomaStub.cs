﻿using OTUS_Homework_10_Antipova_prototype_pattern.Models;

namespace OTUS_Homework_10_Antipova_prototype_pattern.Tests.Stubs;

public static class DiplomaStub
{
    public static Diploma Create() =>
        new Diploma()
        {
            Number = "ХОР-54 552298",
            CourseName = "Обработка металлов давлением",
            StudentFullName = "Беляков Сергей Юрьевич",
            StartDate = new DateTime(2023, 06, 26),
            Degree = "Бакалавр",
            FinalExamScore = 53
        };
}
