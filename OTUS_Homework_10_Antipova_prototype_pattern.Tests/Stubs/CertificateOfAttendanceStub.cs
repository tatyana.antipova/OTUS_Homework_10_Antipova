﻿using OTUS_Homework_10_Antipova_prototype_pattern.Models;

namespace OTUS_Homework_10_Antipova_prototype_pattern.Tests.Stubs;

public static class CertificateOfAttendanceStub
{
    public static CertificateOfAttendance Create() =>
        new CertificateOfAttendance()
        {
            Number = "НЕУД 552298",
            CourseName = "Обработка металлов давлением",
            StudentFullName = "Дедушкин Мазай Зайцевич",
            StartDate = new DateTime(2023, 04, 26),
            AttendanceHours = 10
        };
}
