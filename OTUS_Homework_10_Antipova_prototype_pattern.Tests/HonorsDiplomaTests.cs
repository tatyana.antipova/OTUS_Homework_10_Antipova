using OTUS_Homework_10_Antipova_prototype_pattern.Models;
using OTUS_Homework_10_Antipova_prototype_pattern.Tests.Stubs;
using System.Text.Json;

namespace OTUS_Homework_10_Antipova_prototype_pattern.Tests;

[Trait("Category", "Unit")]
public class HonorsDiplomaTests
{
    [Fact]
    public void Clone_Serialize_ShouldBeEqual()
    {
        var testObject = HonorsDiplomaStub.Create();

        var testObjectCloned = testObject.Clone();
        var testObjectMyCloned = testObject.MyClone();

        string testObjectSerialized = JsonSerializer.Serialize(testObject);
        Assert.True(testObjectSerialized.Equals(JsonSerializer.Serialize(testObjectCloned)));
        Assert.True(testObjectSerialized.Equals(JsonSerializer.Serialize(testObjectMyCloned)));
    }

    [Fact]
    public void Clone_ObjectReferences_ShouldNotBeEqual()
    {
        var testObject = HonorsDiplomaStub.Create();

        var testObjectCloned = testObject.Clone();
        var testObjectMyCloned = testObject.MyClone();

        Assert.True(testObject != testObjectCloned);
        Assert.True(testObject != testObjectMyCloned);
    }

    [Fact]
    public void Clone_ObjectFields_BeforeChange_ShouldBeEqual()
    {
        var testObject = HonorsDiplomaStub.Create();

        var testObjectCloned = testObject.Clone();
        var testObjectMyCloned = testObject.MyClone();

        Assert.True(testObject.CourseName.Equals(((HonorsDiploma)testObjectCloned).CourseName));
        Assert.True(testObject.CourseName.Equals((testObjectMyCloned).CourseName));

        Assert.True(testObject.Degree.Equals(((HonorsDiploma)testObjectCloned).Degree));
        Assert.True(testObject.Degree.Equals((testObjectMyCloned).Degree));

        Assert.True(testObject.OfficialSealAvailable == ((HonorsDiploma)testObjectCloned).OfficialSealAvailable);
        Assert.True(testObject.OfficialSealAvailable == (testObjectMyCloned).OfficialSealAvailable);
    }

    [Fact]
    public void Clone_ObjectFields_AfterChange_ShouldNotBeEqual()
    {
        var testObject = HonorsDiplomaStub.Create();

        testObject.CourseName = "old string - 1";
        testObject.Degree = "old string - 2";
        testObject.OfficialSealAvailable = false;

        var testObjectCloned = testObject.Clone();
        var testObjectMyCloned = testObject.MyClone();

        testObject.CourseName = "some new string - 1";
        testObject.Degree = "some new string - 2";
        testObject.OfficialSealAvailable = true;

        Assert.False(testObject.CourseName.Equals(((HonorsDiploma)testObjectCloned).CourseName));
        Assert.False(testObject.CourseName.Equals((testObjectMyCloned).CourseName));

        Assert.False(testObject.Degree.Equals(((HonorsDiploma)testObjectCloned).Degree));
        Assert.False(testObject.Degree.Equals((testObjectMyCloned).Degree));

        Assert.False(testObject.OfficialSealAvailable == ((HonorsDiploma)testObjectCloned).OfficialSealAvailable);
        Assert.False(testObject.OfficialSealAvailable == (testObjectMyCloned).OfficialSealAvailable);
    }    
}