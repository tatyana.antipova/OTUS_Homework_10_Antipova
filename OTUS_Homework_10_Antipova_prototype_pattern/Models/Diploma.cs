﻿using OTUS_Homework_10_Antipova_prototype_pattern.Abstract;

namespace OTUS_Homework_10_Antipova_prototype_pattern.Models;

/// <summary>
/// Диплом.
/// </summary>
public class Diploma : CourseDocumentBase, IMyCloneable<Diploma>
{
    public Diploma() : base()
    {        
    }

    public Diploma(Diploma diploma) : base(diploma)
    {
        Degree = diploma.Degree;
        FinalExamScore = diploma.FinalExamScore;
    }

    public string Degree { get; set; }

    public int FinalExamScore { get; set; }

    public override Diploma MyClone()
    {
        return new Diploma(this);
    }
}
