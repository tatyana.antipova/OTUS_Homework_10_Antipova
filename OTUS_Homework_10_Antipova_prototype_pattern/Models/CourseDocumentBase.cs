﻿using OTUS_Homework_10_Antipova_prototype_pattern.Abstract;

namespace OTUS_Homework_10_Antipova_prototype_pattern.Models;

/// <summary>
/// Базовая модель документа об обучении.
/// </summary>
public abstract class CourseDocumentBase : ICloneable
{
    public CourseDocumentBase()
    {        
    }

    public CourseDocumentBase(CourseDocumentBase courseDocumentBase)
    {
        Number = courseDocumentBase.Number;
        CourseName = courseDocumentBase.CourseName;
        StudentFullName = courseDocumentBase.StudentFullName;
        StartDate = courseDocumentBase.StartDate;
    }

    public CourseDocumentBase(
        string docNumber,
        string courseName,
        DateTime startDate,
        string studentFillName)
    {
        Number = docNumber;
        CourseName = courseName;
        StudentFullName = studentFillName;
        StartDate = startDate;
    }

    public string Number { get; set; }

    public string CourseName { get; set; }

    public string StudentFullName { get; set; }

    public DateTime StartDate { get; set; }

    public abstract CourseDocumentBase MyClone();

    public virtual object Clone()
    {
        return MyClone();
    }
}
