﻿using OTUS_Homework_10_Antipova_prototype_pattern.Abstract;

namespace OTUS_Homework_10_Antipova_prototype_pattern.Models;

/// <summary>
/// Диплом с отличием.
/// </summary>
public class HonorsDiploma : Diploma, IMyCloneable<HonorsDiploma>
{
    public HonorsDiploma() : base()
    {
    }
    public HonorsDiploma(HonorsDiploma diploma) 
        : base(diploma)
    {
        OfficialSealAvailable = diploma.OfficialSealAvailable;
    }

    public bool OfficialSealAvailable { get; set; }

    public override HonorsDiploma MyClone()
    {
        return new HonorsDiploma(this);
    }
}
