﻿using OTUS_Homework_10_Antipova_prototype_pattern.Abstract;

namespace OTUS_Homework_10_Antipova_prototype_pattern.Models;

/// <summary>
/// Справка об обучении.
/// </summary>
public class CertificateOfAttendance : CourseDocumentBase, IMyCloneable<CertificateOfAttendance>
{
    public CertificateOfAttendance() : base()
    {
    }

    public CertificateOfAttendance(CertificateOfAttendance certificate) : base(certificate)
    {
        AttendanceHours = certificate.AttendanceHours;
    }

    public decimal AttendanceHours { get; set; }

    public override CertificateOfAttendance MyClone()
    {
        return new CertificateOfAttendance(this);
    }
}
