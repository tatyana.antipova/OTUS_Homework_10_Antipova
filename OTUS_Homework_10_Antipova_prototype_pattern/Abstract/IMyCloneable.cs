﻿namespace OTUS_Homework_10_Antipova_prototype_pattern.Abstract;

public interface IMyCloneable<T>
{
    T MyClone();
}
